/*! modernizr 3.3.1 (Custom Build) | MIT *
 * http://modernizr.com/download/?-borderimage-canvas-cssanimations-cssgradients-cssmask-dataset-flexbox-hidden-history-pointerevents-search-svg-target-touchevents-addtest-atrule-domprefixes-fnbind-hasevent-load-mq-prefixed-prefixedcss-prefixes-printshiv-setclasses-testallprops-testprop-teststyles !*/
! function(e, t, n) {
    function r(e, t) {
        return typeof e === t
    }

    function o() {
        var e, t, n, o, i, a, s;
        for (var l in b)
            if (b.hasOwnProperty(l)) {
                if (e = [], t = b[l], t.name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length))
                    for (n = 0; n < t.options.aliases.length; n++) e.push(t.options.aliases[n].toLowerCase());
                for (o = r(t.fn, "function") ? t.fn() : t.fn, i = 0; i < e.length; i++) a = e[i], s = a.split("."), 1 === s.length ? Modernizr[s[0]] = o : (!Modernizr[s[0]] || Modernizr[s[0]] instanceof Boolean || (Modernizr[s[0]] = new Boolean(Modernizr[s[0]])), Modernizr[s[0]][s[1]] = o), E.push((o ? "" : "no-") + s.join("-"))
            }
    }

    function i(e) {
        var t = w.className,
            n = Modernizr._config.classPrefix || "";
        if (C && (t = t.baseVal), Modernizr._config.enableJSClass) {
            var r = new RegExp("(^|\\s)" + n + "no-js(\\s|$)");
            t = t.replace(r, "$1" + n + "js$2")
        }
        Modernizr._config.enableClasses && (t += " " + n + e.join(" " + n), C ? w.className.baseVal = t : w.className = t)
    }

    function a(e, t) {
        if ("object" == typeof e)
            for (var n in e) x(e, n) && a(n, e[n]);
        else {
            e = e.toLowerCase();
            var r = e.split("."),
                o = Modernizr[r[0]];
            if (2 == r.length && (o = o[r[1]]), "undefined" != typeof o) return Modernizr;
            t = "function" == typeof t ? t() : t, 1 == r.length ? Modernizr[r[0]] = t : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = t), i([(t && 0 != t ? "" : "no-") + r.join("-")]), Modernizr._trigger(e, t)
        }
        return Modernizr
    }

    function s(e, t) {
        return !!~("" + e).indexOf(t)
    }

    function l() {
        return "function" != typeof t.createElement ? t.createElement(arguments[0]) : C ? t.createElementNS.call(t, "http://www.w3.org/2000/svg", arguments[0]) : t.createElement.apply(t, arguments)
    }

    function u() {
        var e = t.body;
        return e || (e = l(C ? "svg" : "body"), e.fake = !0), e
    }

    function c(e, n, r, o) {
        var i, a, s, c, f = "modernizr",
            d = l("div"),
            p = u();
        if (parseInt(r, 10))
            for (; r--;) s = l("div"), s.id = o ? o[r] : f + (r + 1), d.appendChild(s);
        return i = l("style"), i.type = "text/css", i.id = "s" + f, (p.fake ? p : d).appendChild(i), p.appendChild(d), i.styleSheet ? i.styleSheet.cssText = e : i.appendChild(t.createTextNode(e)), d.id = f, p.fake && (p.style.background = "", p.style.overflow = "hidden", c = w.style.overflow, w.style.overflow = "hidden", w.appendChild(p)), a = n(d, e), p.fake ? (p.parentNode.removeChild(p), w.style.overflow = c, w.offsetHeight) : d.parentNode.removeChild(d), !!a
    }

    function f(e) {
        return e.replace(/([A-Z])/g, function(e, t) {
            return "-" + t.toLowerCase()
        }).replace(/^ms-/, "-ms-")
    }

    function d(t, r) {
        var o = t.length;
        if ("CSS" in e && "supports" in e.CSS) {
            for (; o--;)
                if (e.CSS.supports(f(t[o]), r)) return !0;
            return !1
        }
        if ("CSSSupportsRule" in e) {
            for (var i = []; o--;) i.push("(" + f(t[o]) + ":" + r + ")");
            return i = i.join(" or "), c("@supports (" + i + ") { #modernizr { position: absolute; } }", function(e) {
                return "absolute" == getComputedStyle(e, null).position
            })
        }
        return n
    }

    function p(e) {
        return e.replace(/([a-z])-([a-z])/g, function(e, t, n) {
            return t + n.toUpperCase()
        }).replace(/^-/, "")
    }

    function m(e, t, o, i) {
        function a() {
            c && (delete _.style, delete _.modElem)
        }
        if (i = r(i, "undefined") ? !1 : i, !r(o, "undefined")) {
            var u = d(e, o);
            if (!r(u, "undefined")) return u
        }
        for (var c, f, m, h, v, g = ["modernizr", "tspan"]; !_.style;) c = !0, _.modElem = l(g.shift()), _.style = _.modElem.style;
        for (m = e.length, f = 0; m > f; f++)
            if (h = e[f], v = _.style[h], s(h, "-") && (h = p(h)), _.style[h] !== n) {
                if (i || r(o, "undefined")) return a(), "pfx" == t ? h : !0;
                try {
                    _.style[h] = o
                } catch (y) {}
                if (_.style[h] != v) return a(), "pfx" == t ? h : !0
            }
        return a(), !1
    }

    function h(e, t) {
        return function() {
            return e.apply(t, arguments)
        }
    }

    function v(e, t, n) {
        var o;
        for (var i in e)
            if (e[i] in t) return n === !1 ? e[i] : (o = t[e[i]], r(o, "function") ? h(o, n || t) : o);
        return !1
    }

    function g(e, t, n, o, i) {
        var a = e.charAt(0).toUpperCase() + e.slice(1),
            s = (e + " " + j.join(a + " ") + a).split(" ");
        return r(t, "string") || r(t, "undefined") ? m(s, t, o, i) : (s = (e + " " + M.join(a + " ") + a).split(" "), v(s, t, n))
    }

    function y(e, t, r) {
        return g(e, n, n, t, r)
    }
    var b = [],
        S = {
            _version: "3.3.1",
            _config: {
                classPrefix: "",
                enableClasses: !0,
                enableJSClass: !0,
                usePrefixes: !0
            },
            _q: [],
            on: function(e, t) {
                var n = this;
                setTimeout(function() {
                    t(n[e])
                }, 0)
            },
            addTest: function(e, t, n) {
                b.push({
                    name: e,
                    fn: t,
                    options: n
                })
            },
            addAsyncTest: function(e) {
                b.push({
                    name: null,
                    fn: e
                })
            }
        },
        Modernizr = function() {};
    Modernizr.prototype = S, Modernizr = new Modernizr;
    var x, E = [],
        w = t.documentElement,
        C = "svg" === w.nodeName.toLowerCase();
    ! function() {
        var e = {}.hasOwnProperty;
        x = r(e, "undefined") || r(e.call, "undefined") ? function(e, t) {
            return t in e && r(e.constructor.prototype[t], "undefined")
        } : function(t, n) {
            return e.call(t, n)
        }
    }(), S._l = {}, S.on = function(e, t) {
        this._l[e] || (this._l[e] = []), this._l[e].push(t), Modernizr.hasOwnProperty(e) && setTimeout(function() {
            Modernizr._trigger(e, Modernizr[e])
        }, 0)
    }, S._trigger = function(e, t) {
        if (this._l[e]) {
            var n = this._l[e];
            setTimeout(function() {
                var e, r;
                for (e = 0; e < n.length; e++)(r = n[e])(t)
            }, 0), delete this._l[e]
        }
    }, Modernizr._q.push(function() {
        S.addTest = a
    });
    C || ! function(e, t) {
        function n(e, t) {
            var n = e.createElement("p"),
                r = e.getElementsByTagName("head")[0] || e.documentElement;
            return n.innerHTML = "x<style>" + t + "</style>", r.insertBefore(n.lastChild, r.firstChild)
        }

        function r() {
            var e = C.elements;
            return "string" == typeof e ? e.split(" ") : e
        }

        function o(e, t) {
            var n = C.elements;
            "string" != typeof n && (n = n.join(" ")), "string" != typeof e && (e = e.join(" ")), C.elements = n + " " + e, u(t)
        }

        function i(e) {
            var t = w[e[x]];
            return t || (t = {}, E++, e[x] = E, w[E] = t), t
        }

        function a(e, n, r) {
            if (n || (n = t), v) return n.createElement(e);
            r || (r = i(n));
            var o;
            return o = r.cache[e] ? r.cache[e].cloneNode() : S.test(e) ? (r.cache[e] = r.createElem(e)).cloneNode() : r.createElem(e), !o.canHaveChildren || b.test(e) || o.tagUrn ? o : r.frag.appendChild(o)
        }

        function s(e, n) {
            if (e || (e = t), v) return e.createDocumentFragment();
            n = n || i(e);
            for (var o = n.frag.cloneNode(), a = 0, s = r(), l = s.length; l > a; a++) o.createElement(s[a]);
            return o
        }

        function l(e, t) {
            t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function(n) {
                return C.shivMethods ? a(n, e, t) : t.createElem(n)
            }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + r().join().replace(/[\w\-:]+/g, function(e) {
                return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
            }) + ");return n}")(C, t.frag)
        }

        function u(e) {
            e || (e = t);
            var r = i(e);
            return !C.shivCSS || h || r.hasCSS || (r.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), v || l(e, r), e
        }

        function c(e) {
            for (var t, n = e.getElementsByTagName("*"), o = n.length, i = RegExp("^(?:" + r().join("|") + ")$", "i"), a = []; o--;) t = n[o], i.test(t.nodeName) && a.push(t.applyElement(f(t)));
            return a
        }

        function f(e) {
            for (var t, n = e.attributes, r = n.length, o = e.ownerDocument.createElement(_ + ":" + e.nodeName); r--;) t = n[r], t.specified && o.setAttribute(t.nodeName, t.nodeValue);
            return o.style.cssText = e.style.cssText, o
        }

        function d(e) {
            for (var t, n = e.split("{"), o = n.length, i = RegExp("(^|[\\s,>+~])(" + r().join("|") + ")(?=[[\\s,>+~#.:]|$)", "gi"), a = "$1" + _ + "\\:$2"; o--;) t = n[o] = n[o].split("}"), t[t.length - 1] = t[t.length - 1].replace(i, a), n[o] = t.join("}");
            return n.join("{")
        }

        function p(e) {
            for (var t = e.length; t--;) e[t].removeNode()
        }

        function m(e) {
            function t() {
                clearTimeout(a._removeSheetTimer), r && r.removeNode(!0), r = null
            }
            var r, o, a = i(e),
                s = e.namespaces,
                l = e.parentWindow;
            return !N || e.printShived ? e : ("undefined" == typeof s[_] && s.add(_), l.attachEvent("onbeforeprint", function() {
                t();
                for (var i, a, s, l = e.styleSheets, u = [], f = l.length, p = Array(f); f--;) p[f] = l[f];
                for (; s = p.pop();)
                    if (!s.disabled && T.test(s.media)) {
                        try {
                            i = s.imports, a = i.length
                        } catch (m) {
                            a = 0
                        }
                        for (f = 0; a > f; f++) p.push(i[f]);
                        try {
                            u.push(s.cssText)
                        } catch (m) {}
                    }
                u = d(u.reverse().join("")), o = c(e), r = n(e, u)
            }), l.attachEvent("onafterprint", function() {
                p(o), clearTimeout(a._removeSheetTimer), a._removeSheetTimer = setTimeout(t, 500)
            }), e.printShived = !0, e)
        }
        var h, v, g = "3.7.3",
            y = e.html5 || {},
            b = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
            S = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
            x = "_html5shiv",
            E = 0,
            w = {};
        ! function() {
            try {
                var e = t.createElement("a");
                e.innerHTML = "<xyz></xyz>", h = "hidden" in e, v = 1 == e.childNodes.length || function() {
                    t.createElement("a");
                    var e = t.createDocumentFragment();
                    return "undefined" == typeof e.cloneNode || "undefined" == typeof e.createDocumentFragment || "undefined" == typeof e.createElement
                }()
            } catch (n) {
                h = !0, v = !0
            }
        }();
        var C = {
            elements: y.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output picture progress section summary template time video",
            version: g,
            shivCSS: y.shivCSS !== !1,
            supportsUnknownElements: v,
            shivMethods: y.shivMethods !== !1,
            type: "default",
            shivDocument: u,
            createElement: a,
            createDocumentFragment: s,
            addElements: o
        };
        e.html5 = C, u(t);
        var T = /^$|\b(?:all|print)\b/,
            _ = "html5shiv",
            N = !v && function() {
                var n = t.documentElement;
                return !("undefined" == typeof t.namespaces || "undefined" == typeof t.parentWindow || "undefined" == typeof n.applyElement || "undefined" == typeof n.removeNode || "undefined" == typeof e.attachEvent)
            }();
        C.type += " print", C.shivPrint = m, m(t), "object" == typeof module && module.exports && (module.exports = C)
    }("undefined" != typeof e ? e : this, t);
    var T = {
        elem: l("modernizr")
    };
    Modernizr._q.push(function() {
        delete T.elem
    });
    var _ = {
        style: T.elem.style
    };
    Modernizr._q.unshift(function() {
        delete _.style
    });
    var N = (S.testProp = function(e, t, r) {
            return m([e], n, t, r)
        }, "Moz O ms Webkit"),
        j = S._config.usePrefixes ? N.split(" ") : [];
    S._cssomPrefixes = j;
    var z = function(t) {
        var r, o = q.length,
            i = e.CSSRule;
        if ("undefined" == typeof i) return n;
        if (!t) return !1;
        if (t = t.replace(/^@/, ""), r = t.replace(/-/g, "_").toUpperCase() + "_RULE", r in i) return "@" + t;
        for (var a = 0; o > a; a++) {
            var s = q[a],
                l = s.toUpperCase() + "_" + r;
            if (l in i) return "@-" + s.toLowerCase() + "-" + t
        }
        return !1
    };
    S.atRule = z;
    var M = S._config.usePrefixes ? N.toLowerCase().split(" ") : [];
    S._domPrefixes = M;
    var k = function() {
        function e(e, t) {
            var o;
            return e ? (t && "string" != typeof t || (t = l(t || "div")), e = "on" + e, o = e in t, !o && r && (t.setAttribute || (t = l("div")), t.setAttribute(e, ""), o = "function" == typeof t[e], t[e] !== n && (t[e] = n), t.removeAttribute(e)), o) : !1
        }
        var r = !("onblur" in t.documentElement);
        return e
    }();
    S.hasEvent = k;
    var A = function() {},
        P = function() {};
    e.console && (A = function() {
        var t = console.error ? "error" : "log";
        e.console[t].apply(e.console, Array.prototype.slice.call(arguments))
    }, P = function() {
        var t = console.warn ? "warn" : "log";
        e.console[t].apply(e.console, Array.prototype.slice.call(arguments))
    }), S.load = function() {
        "yepnope" in e ? (P("yepnope.js (aka Modernizr.load) is no longer included as part of Modernizr. yepnope appears to be available on the page, so we’ll use it to handle this call to Modernizr.load, but please update your code to use yepnope directly.\n See http://github.com/Modernizr/Modernizr/issues/1182 for more information."), e.yepnope.apply(e, [].slice.call(arguments, 0))) : A("yepnope.js (aka Modernizr.load) is no longer included as part of Modernizr. Get it from http://yepnopejs.com. See http://github.com/Modernizr/Modernizr/issues/1182 for more information.")
    };
    var O = function() {
        var t = e.matchMedia || e.msMatchMedia;
        return t ? function(e) {
            var n = t(e);
            return n && n.matches || !1
        } : function(t) {
            var n = !1;
            return c("@media " + t + " { #modernizr { position: absolute; } }", function(t) {
                n = "absolute" == (e.getComputedStyle ? e.getComputedStyle(t, null) : t.currentStyle).position
            }), n
        }
    }();
    S.mq = O, S.testAllProps = g;
    var F = S.prefixed = function(e, t, n) {
            return 0 === e.indexOf("@") ? z(e) : (-1 != e.indexOf("-") && (e = p(e)), t ? g(e, t, n) : g(e, "pfx"))
        },
        q = S._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : [];
    S._prefixes = q;
    S.prefixedCSS = function(e) {
        var t = F(e);
        return t && f(t)
    };
    S.testAllProps = y;
    var D = S.testStyles = c;
    Modernizr.addTest("pointerevents", function() {
        var e = !1,
            t = M.length;
        for (e = Modernizr.hasEvent("pointerdown"); t-- && !e;) k(M[t] + "pointerdown") && (e = !0);
        return e
    }), Modernizr.addTest("touchevents", function() {
        var n;
        if ("ontouchstart" in e || e.DocumentTouch && t instanceof DocumentTouch) n = !0;
        else {
            var r = ["@media (", q.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
            D(r, function(e) {
                n = 9 === e.offsetTop
            })
        }
        return n
    }), Modernizr.addTest("dataset", function() {
        var e = l("div");
        return e.setAttribute("data-a-b", "c"), !(!e.dataset || "c" !== e.dataset.aB)
    }), Modernizr.addTest("history", function() {
        var t = navigator.userAgent;
        return -1 === t.indexOf("Android 2.") && -1 === t.indexOf("Android 4.0") || -1 === t.indexOf("Mobile Safari") || -1 !== t.indexOf("Chrome") || -1 !== t.indexOf("Windows Phone") ? e.history && "pushState" in e.history : !1
    }), Modernizr.addTest("target", function() {
        var t = e.document;
        if (!("querySelectorAll" in t)) return !1;
        try {
            return t.querySelectorAll(":target"), !0
        } catch (n) {
            return !1
        }
    }), Modernizr.addTest("canvas", function() {
        var e = l("canvas");
        return !(!e.getContext || !e.getContext("2d"))
    }), Modernizr.addTest("cssanimations", y("animationName", "a", !0)), Modernizr.addTest("inputsearchevent", k("search")), Modernizr.addTest("svg", !!t.createElementNS && !!t.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect), Modernizr.addTest("flexbox", y("flexBasis", "1px", !0)), Modernizr.addTest("hidden", "hidden" in l("a")), Modernizr.addTest("borderimage", y("borderImage", "url() 1", !0)), Modernizr.addTest("cssgradients", function() {
        for (var e, t = "background-image:", n = "gradient(linear,left top,right bottom,from(#9f9),to(white));", r = "", o = 0, i = q.length - 1; i > o; o++) e = 0 === o ? "to " : "", r += t + q[o] + "linear-gradient(" + e + "left top, #9f9, white);";
        Modernizr._config.usePrefixes && (r += t + "-webkit-" + n);
        var a = l("a"),
            s = a.style;
        return s.cssText = r, ("" + s.backgroundImage).indexOf("gradient") > -1
    }), Modernizr.addTest("cssmask", y("maskRepeat", "repeat-x", !0)), o(), i(E), delete S.addTest, delete S.addAsyncTest;
    for (var L = 0; L < Modernizr._q.length; L++) Modernizr._q[L]();
    e.Modernizr = Modernizr
}(window, document);